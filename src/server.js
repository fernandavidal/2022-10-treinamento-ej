import express from "express";
import userRouter from "./controllers/user-controller.js";

const app = express();

app.use(userRouter);

app.get("/", ()=> {
    res.send({hello: 'world'})
})

export default app;
